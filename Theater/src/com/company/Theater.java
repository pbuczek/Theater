package com.company;

import java.util.Set;
import java.util.TreeSet;

public class Theater {
    private final String name;
    private Set<RowOfSeats> rowsSet;

    public Theater(String name) {
        this.name = name;
        rowsSet = new TreeSet<>();
    }

    public String getName() {
        return name;
    }

    public void addRow(String rowName, int initialNumberOfSeats) {
        RowOfSeats newRow = new RowOfSeats(rowName, initialNumberOfSeats);
        rowsSet.add(newRow);
    }

    public boolean deleteRow(String row) {
        for (RowOfSeats r : rowsSet) {
            if (r.getName().equals(row)) {
                System.out.println("Now removing row " + r.name);
                return rowsSet.remove(r);
            }
        }
        System.out.println("Row with name '" + row + "' not found");
        return false;
    }


    public String addSeats(String row, int numberOfSeats) {
        for (RowOfSeats r : rowsSet) {
            if (r.getName().equals(row)) {
                return r.innerAddSeats(numberOfSeats);
            }
        }
        return "Row with name '" + row + "' not found";
    }

    public String removeSeats(String row, int numberOfSeats) {
        for (RowOfSeats r : rowsSet) {
            if (r.getName().equals(row)) {
                return r.innerRemoveSeats(numberOfSeats);
            }
        }
        return "Row with name '" + row + "' not found";
    }

    public boolean printRow(String row) {
        for (RowOfSeats r : rowsSet) {
            if (r.getName().equals(row)) {
                r.printSeats();
                return true;
            }
        }
        System.out.println("Row with name '" + row + "' not found");
        return false;
    }

    public void printTheater() {
        System.out.println("Theater: " + name);
        for (RowOfSeats r : rowsSet) {
            r.printSeats();
        }
    }

    public boolean reserveSeat(String row, int seatNumber) {
        for(RowOfSeats r: rowsSet) {
            if(r.name.equals(row)) {
                return r.innerReserveSeat(seatNumber);
            }
        }
        System.out.println("Row with name '" + row + "' not found");
        return false;
    }

    public void cancelReservation(String row, int seatNumber) {
        for(RowOfSeats r: rowsSet) {
            if(r.name.equals(row)) {
                r.innerCancelReservation(seatNumber);
                return;
            }
        }
        System.out.println("Row with name '" + row + "' not found");
    }


    private class RowOfSeats implements Comparable {
        private final String name;
        private Set<Seat> seatsList;

        private RowOfSeats(String name, int initialNumberOfSeats) {
            this.name = name;
            seatsList = new TreeSet<>();
            for (int i = 1; i <= initialNumberOfSeats; i++) {
                Seat seat = new Seat(i);
                seatsList.add(seat);
            }
        }

        private String getName() {
            return name;
        }

        private String innerAddSeats(int numberOfSeats) {
            for (int i = 1; i <= numberOfSeats; i++) {
                int numb = seatsList.size() + 1;
                Seat seat = new Seat(numb);
                seatsList.add(seat);
            }
            return (numberOfSeats + " seats added to rowsSet " + this.getName() + ". total number of seats = " + seatsList.size());
        }

        private String innerRemoveSeats(int numberOfSeats) {
            if (numberOfSeats <= seatsList.size()) {
                for (int i = 1; i <= numberOfSeats; i++) {
                    Seat seat = new Seat(seatsList.size());
                    seatsList.remove(seat);
                }
                return (numberOfSeats + " seats deleted from row " + this.getName() + ". total number of seats = " + seatsList.size());
            }
            return "Error";
        }


        private void printSeats() {
            System.out.print("Row " + name + ":  ");
            if (seatsList.size() > 0) {
                System.out.print(name + "1");
                for (int i = 1; i < seatsList.size(); i++) {
                    System.out.print(", " + name + (i + 1));
                }
            }
            System.out.println();
        }

        private boolean innerReserveSeat(int number) {
            for(Seat s: seatsList) {
                if(s.number == number) {
                    if(!s.reserved) {
                        System.out.println("Successfully reserved seat " + this.name + number);
                        return s.reserved = true;
                    } else {
                        System.out.println("Seat " + this.name + number + " already reserved.");
                        return false;
                    }
                }
            }
            System.out.println("Row " + name + " doesn't have seat number " + number);
            return false;
        }

        private void innerCancelReservation(int number) {
            for(Seat s: seatsList) {
                if(s.number == number) {
                    if(s.reserved) {
                        s.reserved=false;
                        System.out.println("Successfully cancelled reservation of seat " + this.name + number);
                    } else {
                        System.out.println("Seat " + this.name + number + " was not reserved");
                    }
                    return;
                }
            }
            System.out.println("Row " + name + " doesn't have seat number " + number);
        }

        @Override
        public int compareTo(Object o) {
            RowOfSeats row = (RowOfSeats) o;
            return this.name.compareTo(row.name);
        }

        private class Seat implements Comparable {
            private final int number;
            private boolean reserved;

            private Seat(int number) {
                this.number = number;
            }

            @Override
            public int compareTo(Object seat) {
                Seat mObj = (Seat) seat;
                return this.number - mObj.number;
            }
        }
    }


}
