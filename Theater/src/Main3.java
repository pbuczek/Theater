import com.company.Theater;

public class Main3 {

    public static void main(String[] args) {
        System.out.println();
        Theater noweHoryzonty = new Theater("Nowe Horyzonty");
        noweHoryzonty.addRow("A",20);
        noweHoryzonty.addRow("B",5);
        noweHoryzonty.reserveSeat("B",6);
        System.out.println(noweHoryzonty.reserveSeat("B",3));
        noweHoryzonty.reserveSeat("B",3);

        System.out.println();
        System.out.println("##################");
        System.out.println();

        noweHoryzonty.cancelReservation("D",3);
        noweHoryzonty.cancelReservation("B",8);
        noweHoryzonty.cancelReservation("B",3);
        noweHoryzonty.cancelReservation("B",3);
    }
}