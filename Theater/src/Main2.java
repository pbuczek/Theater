import com.company.Theater;

public class Main2 {

    public static void main(String[] args) {
        Theater Multikino = new Theater("Multikino");
        Multikino.addRow("A",0);
        System.out.println(Multikino.printRow("V"));
        Multikino.printRow("A");
        System.out.println();
        Multikino.printTheater();

        System.out.println("------------------------");

        Multikino.addRow("Test", 10);
        System.out.println(Multikino.removeSeats("Test",10));
        Multikino.printRow("Test");
        System.out.println(Multikino.getName());
        Multikino.addSeats("A", 3);
        Multikino.addSeats("Test", 4);
        Multikino.deleteRow("Test");
        System.out.println(Multikino.deleteRow("BadRow"));

        System.out.println();
        Multikino.printTheater();

    }


}
